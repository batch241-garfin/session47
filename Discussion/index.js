
// document - refers to the whole webpage
// querySelector - used to select specific object (HTML elements) from the documents (webpage)

const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");

// document.getElementById
// document.getElementByClass
// document.getElementByTagName
//const txtFirstName = document.getElementById("txt-first-name");
//const spanFullName = document.getElementById("span-full-name");

// Whenever a user interacts with a webpage, this action is considered as event
// addEventListener - function that takes two arguments
// 'keyup' - string identifying the event
// function that the listener will execute once the specified event is triggered.

txtFirstName.addEventListener('keyup', (event) => {

	// innerHTML - property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;
	// console.log(event.target);
	// console.log(event.target.value);
});